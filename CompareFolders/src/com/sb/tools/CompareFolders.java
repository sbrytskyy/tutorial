package com.sb.tools;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class CompareFolders {

	enum Status {
		DESTINATION_BIGGER,
		SOURCE_BIGGER,
		MISSED
	}

	class FileInfo {
		final long sourceSize;
		final String fullpath;
		Status status = Status.MISSED;

		long destinationSize;
		String destinationFullpath;

		public FileInfo(String fullpath, long size) {
			this.sourceSize = size;
			this.fullpath = fullpath;
		}

		@Override
		public String toString() {
			return "[size=" + sourceSize + ":" + destinationSize + ", status=" + status + ", source=" + fullpath
					+ ", destination=" + destinationFullpath + "]";
		}
	}

	private Map<String, FileInfo> files = new HashMap<>();

	public void compare(String path1, String path2) {
		scanFolder(path1, (filePath) -> processSource(filePath));
		scanFolder(path2, (filePath) -> processDestination(filePath));
		printDiffs(path1);
	}

	private void printDiffs(String sourceRoot) {
		System.out.println("--------------------- SOURCE_BIGGER ------------------------------");
		printMapWithStatus(Status.SOURCE_BIGGER);
		System.out.println("\n\n\n");
//		System.out.println("--------------------- DESTINATION_BIGGER ------------------------------");
//		printMapWithStatus(Status.DESTINATION_BIGGER);
//		System.out.println("\n\n\n");
		System.out.println("---------------------  MISSED  ------------------------------");
		printMapWithStatus(Status.MISSED);

		processFiles(sourceRoot, "D:\\uranium\\temp\\missed", Status.MISSED);
		processFiles(sourceRoot, "D:\\uranium\\temp\\missed", Status.SOURCE_BIGGER);
	}

	private void processFiles(String sourceRoot, String destination, Status status) {
		files.entrySet().stream()
				.filter(p -> p.getValue().status.equals(status))
				.forEach((e) -> copyMissedFile(e.getValue().fullpath, sourceRoot, destination));
	}

	private void copyMissedFile(String source, String sourceRoot, String destination) {
		Path src = Paths.get(source);
		String suffix = source.substring(sourceRoot.length());
		Path dest = Paths.get(destination + "\\" + suffix);
		System.out.println(dest);
		try {
			Files.createDirectories(dest.getParent());
			Files.copy(src, dest, StandardCopyOption.COPY_ATTRIBUTES);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	long total = 0;

	private void printMapWithStatus(Status status) {
		total = 0;
		files.entrySet().stream()
				.filter(p -> p.getValue().status.equals(status))
				.forEach((e) -> {
					long sourceSize = e.getValue().sourceSize;
					total += sourceSize;
//					if (sourceSize > 1000000) {
					System.out.println(e.getKey() + ":" + e.getValue());
//					}
				});
		System.out.println("Total size: " + 1.0 * total / 1024 / 1024 + " MB");
	}

	private void scanFolder(String rootPath, Consumer<Path> consumer) {
		try (Stream<Path> paths = Files.walk(Paths.get(rootPath))) {
			paths
					.filter(Files::isRegularFile)
					.filter(p -> !p.getFileName().toString().toLowerCase().endsWith(".json"))
//					.filter(p -> !p.getFileName().toString().toLowerCase().endsWith(".mp4"))
//					.filter(p -> !p.getFileName().toString().toLowerCase().endsWith(".mov"))
//					.filter(p -> !p.getFileName().toString().toLowerCase().endsWith(".avi"))
					.forEach(consumer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void processSource(Path filePath) {
		try {
			long fileSize = Files.size(filePath);
			String filename = filePath.getFileName().toString();
			files.put(filename, new FileInfo(filePath.toString(), fileSize));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void processDestination(Path filePath) {
		try {
			long fileSize = Files.size(filePath);
			String filename = filePath.getFileName().toString();

			if (files.containsKey(filename)) {
				FileInfo fileInfo = files.get(filename);
				if (fileInfo.sourceSize == fileSize) {
					files.remove(filename);
				} else {
					fileInfo.status = fileInfo.sourceSize > fileSize ? Status.SOURCE_BIGGER : Status.DESTINATION_BIGGER;
					fileInfo.destinationSize = fileSize;
					fileInfo.destinationFullpath = filePath.toString();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws IOException {
		CompareFolders cf = new CompareFolders();
		cf.compare("D:\\uranium\\Backup\\google.photos\\Google Photos", "D:\\uranium\\Drive\\Google Photos");
	}
}
