package com.sb.test.combinatorics;

import java.util.Arrays;

public class CombiTest3 {

	private int counter = 0;
	private final static int[] DIGITS = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	public int numberOfNumbers(int n) {
		counter = 0;

		int[] combination = new int[n];

		permute(combination, 0, 0);

		return counter;
	}

	private void permute(int[] combination, int index, int dInd) {
		if (index == combination.length) {
			System.out.println(Arrays.toString(combination));
			counter++;
			return;
		}

		for (int i = dInd; i < DIGITS.length; i++) {
			combination[index] = DIGITS[i];
			permute(combination, index + 1, dInd + 1);
		}
	}

	public static void main(String[] args) {
		CombiTest3 sol = new CombiTest3();

		int n;
		int r;

		n = 3;
		r = sol.numberOfNumbers(n);
		assert r == 9 * 8 * 7 : String.format("Actual result: %d", r);

		n = 4;
		r = sol.numberOfNumbers(n);
		assert r == 9 * 8 * 7 * 6 : String.format("Actual result: %d", r);
	}
}
