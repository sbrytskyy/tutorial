package com.sb.test.combinatorics;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CombiTest1 {

	private int counter = 0;
	private final static int[] DIGITS = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	public int numberOfNumbers(int n) {
		counter = 0;

		int[] combination = new int[n];

		permute(combination, 0);

		return counter;
	}

	private void permute(int[] combination, int index) {
		if (index == combination.length) {

			Set<Integer> check = new HashSet<>();
			for (int num : combination) {
				if (!check.add(num)) {
					return;
				}
			}
			System.out.println(Arrays.toString(combination));
			counter++;
			return;
		}

		for (int i = 0; i < DIGITS.length; i++) {
			combination[index] = DIGITS[i];
			permute(combination, index + 1);
		}
	}

	public static void main(String[] args) {
		CombiTest1 sol = new CombiTest1();

		int n;
		int r;

		n = 3;
		r = sol.numberOfNumbers(n);
		assert r == 9 * 8 * 7 : String.format("Actual result: %d", r);

		n = 4;
		r = sol.numberOfNumbers(n);
		assert r == 9 * 8 * 7 * 6 : String.format("Actual result: %d", r);
	}
}
