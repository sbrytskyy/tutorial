package com.sb.test.combinatorics;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class CombiTest2 {

	private int counter = 0;
	private final static int[] DIGITS = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	public int numberOfNumbers(int n) {
		counter = 0;

		int[] combination = new int[n];

		Set<Integer> digits = new HashSet<>();
		for (int d : DIGITS) {
			digits.add(d);
		}

		permute(combination, 0, digits);

		return counter;
	}

	private void permute(int[] combination, int index, Set<Integer> digits) {
		if (index == combination.length) {

			System.out.println(Arrays.toString(combination));
			counter++;
			return;
		}

		for (int d : digits) {
			combination[index] = d;
			Set<Integer> digitsCopy = new HashSet<>(digits);
			digitsCopy.remove(d);
			permute(combination, index + 1, digitsCopy);
			digits.add(d);
		}
	}

	public static void main(String[] args) {
		CombiTest2 sol = new CombiTest2();

		int n;
		int r;

		n = 3;
		r = sol.numberOfNumbers(n);
		assert r == 9 * 8 * 7 : String.format("Actual result: %d", r);

		n = 4;
		r = sol.numberOfNumbers(n);
		assert r == 9 * 8 * 7 * 6 : String.format("Actual result: %d", r);
	}
}
